# Project: TYPO3 Theme

## Table of contents

- [Getting started](#getting-started)
  - [Setup](#setup)
- [Built with](#built-with)
- [Authors](#authors)

## Getting started

### Prerequisites

What things you need to install the software and how to install them.

- [Fractal](https://fractal.build/guide/)

### Setup
#### Run
```shell
npm install
```
```shell
npm run build
```
```shell
fractal start --sync
```

#### Watch
```shell
npm run watch
```

## Built with
- [Node JS: 21](https://nodejs.org/en/docs/)

## Build CSS and JS (watcher is on)
````shell
npm run build
````

## Authors

- **Andreas Reichel** - _Initial work_
- **Andreas Reichel** - _Bug fixes_
- **Andreas Reichel** - _Feature development_