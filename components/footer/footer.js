let footerCopyrightYear = document.querySelector('#footerCopyrightYear');

if(footerCopyrightYear) {
    footerCopyrightYear.innerHTML = new Date().getFullYear();
}
