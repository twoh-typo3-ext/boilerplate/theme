document.addEventListener("DOMContentLoaded", function () {
    var carousel = document.querySelector('#headline-slider');
    var slides = carousel.querySelectorAll('.carousel-item');
    var totalSlides = slides.length;
    var currentIndex = 0; // Startindex
    var beforeIndex = 0; // Startindex
    var slideInterval = 3000; // Interval in Millisekunden (3 Sekunden)
    var header = document.getElementsByTagName("header")[0];
    header.classList.add("vh-100");
    function updateSlideCounter() {
        var counter = document.querySelector('.slide-counter');
        counter.textContent = ("0" + (currentIndex + 1)) + " / " + ("0" + totalSlides);
    }

    function goToSlide(index, direction) {
        /*if(direction == 'next'){*/
            slides[currentIndex].classList.remove('active');
            slides[currentIndex].classList.add('to-next-prev'); // Neue Klasse für den vorherigen Slide
            currentIndex = (index + totalSlides) % totalSlides; // Index aktualisieren nach dem Entfernen der Klasse
            slides[currentIndex].classList.add('active');
            beforeIndex = (index-1 + totalSlides) % totalSlides; // Index aktualisieren nach dem Entfernen der Klasse
            setTimeout(function() {
                slides[beforeIndex].classList.remove('to-next-prev');
            }, 2000);
            updateSlideCounter();

/*        }
        else if(direction == 'prev'){
            slides[currentIndex].classList.remove('active');
            slides[currentIndex].classList.add('to-prev-prev'); // Neue Klasse für den vorherigen Slide
            currentIndex = (index + totalSlides) % totalSlides; // Index aktualisieren nach dem Entfernen der Klasse
            slides[currentIndex].classList.add('active');
            beforeIndex = (index-1 + totalSlides) % totalSlides; // Index aktualisieren nach dem Entfernen der Klasse
            setTimeout(function() {
                slides[beforeIndex].classList.remove('to-prev-prev');
            }, 2000);
            updateSlideCounter();
        }*/
    }


    function nextSlide() {
        goToSlide(currentIndex + 1, 'next');
    }

    function prevSlide() {
        goToSlide(currentIndex - 1, 'prev');
    }

    const nextButton = document.querySelector('.next-button');
    if (nextButton) {
        nextButton.addEventListener('click', nextSlide);
    }

    const prevButton = document.querySelector('.prev-button');
    if (prevButton) {
        prevButton.addEventListener('click', prevSlide);
    }

    updateSlideCounter();
    /*startSlideInterval();*/

    const downArrow = document.querySelector('.down-arrow');
    if (downArrow) {
        downArrow.addEventListener('click', () => {
            window.scrollTo({ top: downArrow.offsetTop, behavior: 'smooth'});
        });
    }
});
