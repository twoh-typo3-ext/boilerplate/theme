let accordionClose = document.getElementsByClassName('auto-close');

if(accordionClose.length > 0) {
    let accordion = document.getElementsByClassName('accordion');
    let accordionId = document.querySelector('.accordion').id;
    console.log(accordionId);

    if(accordion.length > 0) {
        Array.from(accordion).forEach(function (accordionItem) {
            let accordionCollapseElements = accordionItem.querySelectorAll('.accordion-collapse');

            accordionCollapseElements.forEach(function (accordionCollapseElement) {
                accordionCollapseElement.setAttribute('data-bs-parent', '#' + accordionId);
            });
        });
    }
}
