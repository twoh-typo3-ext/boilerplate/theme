const galerieItems = document.querySelectorAll('.galerie-item');
const borderClass = 'no-border-item';
const galerieImagesPerLine = 6;

if (galerieItems.length > 0) {
    const totalElements = galerieItems.length;
    const elementsInLastRow = totalElements % galerieImagesPerLine || galerieImagesPerLine;
    const startIndex = totalElements - elementsInLastRow;

    for (let i = startIndex; i < totalElements; i++) {
        galerieItems[i].classList.remove(borderClass);
    }
}