const listItems = document.querySelectorAll('.submenu');
const navbar = document.querySelector('.navbar');
const menuSecond = document.getElementById('menu-second');

if (listItems) {
    listItems.forEach((item) => {
        item.addEventListener('click', (event) => {
            const icon = document.getElementById("submenu-icon");
            icon.classList.toggle('d-none');
            const iconCloseSubmenu = document.getElementById("close-submenu-icon");
            iconCloseSubmenu.classList.toggle('d-none');


            const target = document.querySelector(item.getAttribute('data-click'));
            const submenu = document.getElementById('submenu');
            const searchNav = document.getElementById('search-field');

            if((target !== null && target.id === 'close-submenu-icon') || item.id === 'close-submenu-icon-resp'){
                target.classList.add('d-none');
                searchNav.classList.add('d-none');
                submenu.classList.add('d-none');
                navbar.classList.remove('open');
                firstLevelsA.forEach((mainHeadline) => {
                    mainHeadline.classList.remove('active');
                });
            }else if ((target !== null && target.id === 'search-field')){
                navbar.classList.add('open');
                searchNav.classList.remove('d-none');
                submenu.classList.add('d-none');
                icon.classList.add('d-none');
                iconCloseSubmenu.classList.remove('d-none');
            }
            else if(target !== null){
                target.classList.remove('d-none');
                navbar.classList.add('open');
            }


            if (navbar.classList.contains('open') && mediaQueryList.matches) {
                menuSecond.classList.add('d-none');
            } else {
                menuSecond.classList.remove('d-none');
            }
            event.preventDefault();
        });
    });
}

const mediaQueryList = window.matchMedia("(max-width: 767px)");
const secondLevels = document.querySelectorAll('.second-level');
const firstLevels = document.querySelectorAll('.first-level');
const firstLevelsA = document.querySelectorAll('.main-headline');
const footLevelUl = document.querySelector('.foot-level-ul');
const footLevel = document.querySelector('.foot-level');
const menuContainer = document.querySelector('.menu-container');
const separators = document.querySelectorAll('.separator');
const menuFirst = document.getElementById('menu-first');
const navbarSupportedContentSub = document.getElementById('navbarSupportedContentSub');



function handleMediaQueryChange(mediaQueryList) {
    if (mediaQueryList.matches) {
        secondLevels.forEach((item) => {
            item.classList.add('d-none');
            item.classList.remove('mt-3');
            item.classList.add('py-1');
        });
        firstLevels.forEach((item) => {
            item.classList.remove('d-flex');
            item.classList.remove('flex-wrap');
            item.classList.remove('gap-2');
        });
        footLevelUl.classList.add('d-flex');
        footLevelUl.classList.add('justify-content-start');

        footLevel.classList.remove('justify-content-end');
        footLevel.classList.add('justify-content-start');

        menuContainer.classList.remove('mx-5');
        menuContainer.classList.add('ml-5');

        separators.forEach((item) => {
            item.classList.remove('d-none');
        });

        menuFirst.classList.add('d-none');
        navbarSupportedContentSub.classList.remove('d-none');

        if(navbar.classList.contains('open')){
            menuSecond.classList.add('d-none');
        }

        firstLevelsA.forEach((item, index) => {
            item.addEventListener('click', function(event) {
                const clickedSecondLevel = secondLevels[index];
                clickedSecondLevel.classList.toggle('d-none');

                secondLevels.forEach((secondLevel, secondIndex) => {
                    if (secondIndex !== index && !secondLevel.classList.contains('d-none')) {
                        secondLevel.classList.add('d-none');
                    }
                });

                firstLevelsA.forEach((mainHeadline) => {
                    mainHeadline.classList.remove('active');
                });

                item.classList.add('active');
            });
        });


    } else {
        secondLevels.forEach((item) => {
            item.classList.remove('d-none');
        });
        firstLevels.forEach((item) => {
            item.classList.add('d-flex');
            item.classList.add('flex-wrap');
            item.classList.add('gap-2');
        });
        menuSecond.classList.remove('d-none');

        menuFirst.classList.remove('d-none');
        navbarSupportedContentSub.classList.add('d-none');

    }
}



handleMediaQueryChange(mediaQueryList);

mediaQueryList.addEventListener('change', handleMediaQueryChange)
