calculatePressPageHeight();
window.addEventListener('resize', function(event) {
    calculatePressPageHeight();
}, true);


function calculatePressPageHeight()
{
    let newsDetail = document.querySelector('#news-detail');
    if (newsDetail) {
        let newsDetailHeight = newsDetail.clientHeight;
        newsDetailHeight = (newsDetailHeight / 100) * 75;
        document.querySelector('main').style.height = newsDetailHeight + "px";
    }
}