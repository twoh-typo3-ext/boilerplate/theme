window.addEventListener('load', function() {
    adjustContainerHeight();
});

function adjustContainerHeight() {
    var rows = document.querySelectorAll('.iconcardcontainer');

    rows.forEach(function(row) {
        var containers = row.querySelectorAll('.iconcard');
        var maxHeight = 0;

        containers.forEach(function(container) {
            var containerHeight = container.offsetHeight;
            if (containerHeight > maxHeight) {
                maxHeight = containerHeight;
            }
        });

        containers.forEach(function(container) {
            container.style.height = maxHeight + 'px';
            container.style.width = maxHeight + 'px';
        });
    });
}
